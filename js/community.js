var CommunityListModule = React.createClass({
  getInitialState: function() {
    return {
      limit:'20',
      type: 'recent',
      answer_status: 'verified',
      reason: "",
      targetSelect: "",
      userData: this.props.userData,
      interestData: [],
    };
  },

  handleSuccess: function(details){
    var partialState = {};
    partialState['name'] = '';
    partialState['image'] = '';
    partialState['bgColor'] = '#000000';
    partialState['bimage'] = '';
    partialState['btext'] = '';
    this.setState(partialState);
  },

  handleError: function(xhr, status, err){
    console.error(this.props.url, status, err.toString());
    var partialState = {};
    partialState['message'] = "No";
    this.setState(partialState);
  },

  handleSubmit: function(e) {
    //e.preventDefault();
    var taskId = this.state.userData.task;
    var status = this.state.answer_status;
    var reason = this.state.reason;
    var access_token = this.state.userData.token;
    var formData = {taskId:taskId, status: status, reason: reason, access_token: access_token};
    this.handleFormSubmit(formData);
    return;
  },

  handleFormSubmit: function(formData) {
    $.ajax({
      type: 'POST',
      url: this.state.userData.serverURL+this.state.userData.responseApi+this.state.userData.community,
      data: formData,
      success: function(data) {
        this.handleSuccess(data);
      }.bind(this),
      error: function(xhr, status, err) {
        this.handleError(xhr, status, err);
      }.bind(this)
    });
  },

  handleInputChange: function(key, event) {
    var partialState = {};
    var closestLi = $(event.target).closest('li');
    if($(event.target).is('select') && closestLi.length){
      partialState['targetSelect'] = closestLi.attr('id');
    }
    partialState[key] = event.target.value;
    this.setState(partialState);
  },

  handleClick: function(e){
    var target = $(e.target).closest('li');
    if($(e.target).hasClass('edit')){
      this.state.userData['interest'] = target.attr('id');
      this.setState(this.state.userData);
      React.render(
        <InterestModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else if($(e.target).hasClass('add')){
      this.state.userData['interest'] = '';
      this.setState(this.state.userData);
      React.render(
        <InterestModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else{ 
    this.state.userData['community'] = target.attr('id');
    //this.state.userData['communityData'] = this.state.interestData;
    this.setState(this.state.userData);
    React.unmountComponentAtNode(document.getElementById('contentArea'));
    React.render(
      <MessageModule userData={this.state.userData}/>,
      document.getElementById('contentArea')
    );
  }
  },

  getDataFromServer: function(data){
    var formData = {
      access_token: this.state.userData.token,
      type: this.state.type,
      limit: this.state.limit,
    }
    $.extend(formData, data);
    $.ajax({
      url: this.state.userData.serverURL+this.state.userData.taskApi+this.state.userData.task+this.state.userData.communityApi,
      type: 'GET',
      data: formData,
      dataType: 'json',
      success: function(data) {
        var partialState = {};
        this.state.userData['communityData'] = data;
        this.setState(this.state.userData);
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  componentDidMount: function() {
    var data = {};    
    if(this.state.userData.communityData === undefined)
      this.getDataFromServer(data);
  },

  handlePageClick: function(key){
    var data = {};
    if(this.state.userData.communityData !== undefined && key == 'prev') { 
      data['previous'] = this.state.userData.communityData.previous;
    }
    else if(this.state.userData.communityData !== undefined && key == 'next'){
      data['next'] = this.state.userData.communityData.next;
    }
    this.getDataFromServer(data);
  },

  render: function() {
    var limit = this.state.limit;
    var type = this.state.type;
    var taskId = this.state.userData.task;
    var answer_status = this.state.answer_status;
    var needVerify = false;
    var reason = this.state.reason;
    var targetSelect = this.state.targetSelect;

    var taskData = this.state.userData.taskData === undefined ? '' : this.state.userData.taskData.map(function (task) {
      if(task._id == taskId && task.verify== false){
        needVerify = true;
      }
    });

    var InterestList = this.state.userData.communityData === undefined ? '' : this.state.userData.communityData.answers.map(function (community) {
      var user =  <div> 
                    <span>
                      <img className="dashboard-avatar" src={community.image} width='50' height='50' />
                    </span>
                    <span>
                      {community.name}
                    </span>
                  </div>;
      var verifyOptions = "";
      var submitOption = "";
      var myclass = "";
      if(community.status == 'PENDING'){
        verifyOptions = <select value={this.answer_status} onChange={this.handleInputChange.bind(null, 'answer_status')} >
          <option value='verified'> Verify </option>
          <option value='rejected'> Reject </option>
        </select>
        submitOption = <input type='button' value='Update' onClick={this.handleSubmit}/>
        myclass = 'pending';
      }
      var reasonArea = "";
      if(community.status == 'PENDING' && answer_status == 'rejected' && targetSelect == community.response.answer_id) {
        reasonArea = <span> <br /> <textarea value={this.reason} onChange={this.handleInputChange.bind(null, 'reason')} /></span>
      }

      if(needVerify){
        verifyOptions = 
          <span> {community.status} {verifyOptions}  {reasonArea} {submitOption}</span>
      }
      return(
        <li className={myclass} id={community.response.answer_id}> {user} Status: {verifyOptions}<br /><br /></li>

      );
    }.bind(this));
    var nextLink = "";
    var prevLink = "";
    if(this.state.userData.communityData !== undefined && this.state.userData.communityData.next)
      nextLink = <span className='next' onClick={this.handlePageClick.bind(null, 'next')}>Next</span>;
    if(this.state.userData.communityData !== undefined && this.state.userData.communityData.previous)
      prevLink = <span className='previous'onClick={this.handlePageClick.bind(null, 'prev')}>Previous</span>;
    return (
      <div className="box-content" id="uploadImageList">
      <div className='community'>
        <div className='options'>
        Limit: <input type="text" value={limit} onChange={this.handleInputChange.bind(null, 'limit')} /><br />
        Type: <select value={type} onChange={this.handleInputChange.bind(null, 'type')} >
          <option value='recent'> Recent </option>
          <option value='score'> Score </option>
        </select>
        <button onClick={this.handlePageClick.bind(null, 'get')}>Get Data</button>
        </div>
        <br />
        <ul className="pager"><li className="previous">{prevLink}</li><li className="next">{nextLink}</li></ul>
        <ul className="dashboard-list" onClick={this.handleClick.bind(null)}>
          {InterestList}
        </ul>
      </div>
      </div>
    );
  }
});


var MessageModule = React.createClass({
  getInitialState: function() {
    return {
      message: "",
      userData: this.props.userData,
    };
  },

  render: function(){
    var mydata = "";
    var communityAnswerID = this.state.userData.community;  
    var InterestList = this.state.userData.communityData === undefined ? '' : this.state.userData.communityData.answers.map(function (community) {
      if(community.response.answer_id == communityAnswerID){
      //var community = this.props.userData;
      var answer_frame = "";
      if(community.response.response.attachment[0].type == "video"){
        answer_frame = 
          <video width="320" height="240" controls autoplay loop>
            <source src={community.response.response.attachment[0].streaming_url} />
            <source src={community.response.response.attachment[0].url} />
            Your browser does not support the video tag.
          </video> 
      }
      else if(community.response.response.attachment[0].type == 'link'){
        answer_frame = <iframe src={community.response.response.attachment[0].url}></iframe>
      }
      else{
        answer_frame = <img src={community.response.response.attachment[0].url} width="450" height="450"/>
      }
      var answer =  <div id={community.response.answer_id}> 
                      {community.response.response.text} 
                      <br /><br />
                      {answer_frame}
                    </div> 
      mydata = answer;
      return(
        <li>Answer: <br />{answer}</li> 
      );
      }
    });
    return(
      <div id='communityDataDiv'>
        {mydata}
      </div>
    );
    
  },
});
