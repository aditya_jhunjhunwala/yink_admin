  var AdminModule = React.createClass({

  getInitialState: function() {
    return {
      userData: this.props.userData,
      interestData: [],
    };
  },

  getDataFromServer: function(key, api){
    var formData = {}
    if(key == 'userListData'){
      formData = {
        access_token: this.state.userData.token,
        limit: 500,
      }
    }
    var url = this.state.userData.serverURL + api;
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      data: formData,
      async: false,
      success: function(data) {
        this.state.userData[key] = data;
        this.setState(this.state.userData);   
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  handleClick: function(key, e){
    //React.unmountComponentAtNode(document.getElementById('dataArea'));
    //React.unmountComponentAtNode(document.getElementById('listArea'));
    if(key == 'interest'){
      this.getDataFromServer('interestData', this.state.userData.interestApi);
      this.getDataFromServer('campaignData', this.state.userData.campaignApi);
      this.getDataFromServer('taskData', this.state.userData.taskApi);
      React.unmountComponentAtNode(document.getElementById('contentArea'));
      React.unmountComponentAtNode(document.getElementById('listArea'));
    
      React.render(
        <InterestListModule userData={this.state.userData}/>,
        document.getElementById('listArea')
      );
    }
    else if(key == 'userHistory'){
      this.getDataFromServer('userListData', this.state.userData.userApi);
      React.unmountComponentAtNode(document.getElementById('contentArea'));
      React.unmountComponentAtNode(document.getElementById('listArea'));
      React.render(
        <UserModule userData={this.state.userData}/>,
        document.getElementById('listArea')
      );
    }
    else if(key == 'uploadImage'){
      this.getDataFromServer('uploadImageList', this.state.userData.imageApi);
      this.getDataFromServer('interestData', this.state.userData.interestApi);
      React.unmountComponentAtNode(document.getElementById('contentArea'));
      React.unmountComponentAtNode(document.getElementById('listArea'));
      React.render(
        <LibraryListModule userData={this.state.userData}/>,
        document.getElementById('listArea')
      );
    }
  },
/*
  componentWillMount: function() {
      this.state.userListData = {};
      this.state.interestData = {};
      this.state.campaignData = {};
      this.state.taskData = {};
  },
*/
  /*
   * Renders the HTML page.
   * This is called on every state update.
   */
  render: function() {
    return (
      <div className="ch-container">
        <div className="row">
        
          <div className="col-sm-2 col-lg-2">
            <div className="sidebar-nav">
              <div className="nav-canvas">
                <div className="nav-sm nav nav-stacked">

                </div>
                <ul className="nav nav-pills nav-stacked main-menu">
                  <li className="nav-header">Main</li>
                  <li><a className="ajax-link" onClick={this.handleClick.bind(null,'interest')}><i className="glyphicon glyphicon-home"></i><span> Get Interest</span></a>
                  </li>
                  <li><a className="ajax-link" onClick={this.handleClick.bind(null,'userHistory')}><i className="glyphicon glyphicon-eye-open"></i><span> User History </span></a>
                  </li>
                  <li><a className="ajax-link" onClick={this.handleClick.bind(null,'uploadImage')}><i className="glyphicon glyphicon-edit"></i><span> Add to Library </span></a></li>
                </ul>
              </div>
            </div>
          </div>

          <noscript>
            <div className="alert alert-block col-md-12">
              <h4 className="alert-heading">Warning!</h4>

              <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.</p>
            </div>
          </noscript>
          <div id="content" className="col-lg-10 col-sm-10">
            <div className="row">
              
              <div className="box col-md-5">
                <div className="box-inner">
                  <div className="box-header well" data-original-title="">
                    <h2><i className="glyphicon glyphicon-user"></i> Items </h2>
                    <div className="box-icon">
                      <a href="#" className="btn btn-minimize btn-round btn-default"><i className="glyphicon glyphicon-chevron-up"></i></a>
                      <a href="#" className="btn btn-close btn-round btn-default"><i className="glyphicon glyphicon-remove"></i></a>
                    </div>
                  </div>
                  <div className="box-content" id="listArea"></div>
                </div>
              </div>

              <div className="box col-md-6">
                <div className="box-inner">
                  <div className="box-header well" data-original-title="">
                    <h2><i className="glyphicon glyphicon-user"></i> Items </h2>
                    <div className="box-icon">
                      <a href="#" className="btn btn-minimize btn-round btn-default"><i className="glyphicon glyphicon-chevron-up"></i></a>
                      <a href="#" className="btn btn-close btn-round btn-default"><i className="glyphicon glyphicon-remove"></i></a>
                    </div>
                  </div>
                  <div className="box-content" id="contentArea"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
