var InterestListModule = React.createClass({
  getInitialState: function() {
    return {
      userData: this.props.userData,
      interestData: [],
    };
  },
  handleClick: function(e){
    React.unmountComponentAtNode(document.getElementById('contentArea'));
    var target = $(e.target).closest('li');
    if($(e.target).hasClass('edit')){
      this.state.userData['interest'] = target.attr('id');
      this.setState(this.state.userData);
      React.render(
        <InterestModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else if($(e.target).hasClass('add')){
      this.state.userData['interest'] = '';
      this.setState(this.state.userData);
      React.render(
        <InterestModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else if($(e.target).hasClass('delete')){
      this.deleteFromServer(target.attr('id'));
      React.render(
        <InterestModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else{ 
      this.state.userData['interest'] = target.attr('id');
      this.setState(this.state.userData);
      React.render(
        <CampaignListModule userData={this.state.userData}/>,
        document.getElementById('listArea')
      );
    }
  },

  getDataFromServer: function(key, api){
    var url = this.state.userData.serverURL + api;
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      async: false,
      success: function(data) {
        this.state.userData[key] = data;
        this.setState(this.state.userData);   
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  deleteFromServer: function(id){
    $.ajax({
      type: 'GET',
      url: this.state.userData.serverURL+this.state.userData.interestApi+'/'+id+this.state.userData.deleteApi,
      dataType: 'application/json',
      success: function(data) {
        this.getDataFromServer('interestData', this.state.userData.interestApi);
      }.bind(this),
      error: function(xhr, status, err) {
        this.getDataFromServer('interestData', this.state.userData.interestApi);
      }.bind(this)
    });
  },
  render: function() {
    var data = this.state.userData;
    var InterestList = data.interestData === undefined ? '' : data.interestData.map(function (interest) {
      var editSpan = <button className=' btn btn-primary btn-xs edit'>Edit </button> ;
      var deleteSpan = <button className='delete btn btn-danger btn-xs'>Delete </button>;
      return(
        /*<li id={interest._id}><div className='leftClass'><img src={interest.image} width="50" height="50" />{interest.name}</div><div className='rightClass'>{editSpan}{deleteSpan}</div></li>*/
        <li id={interest._id}>
          <a href="#">
          <img className="dashboard-avatar" alt={interest.name}
              src={interest.image} /></a>
          <strong>Name:</strong> <a href="#">{interest.name}
          </a><br />
          {editSpan} &nbsp; &nbsp;
          {deleteSpan}
          <br />
        </li>
      );
    });
    return (
            <div className="box-content">
              <ul onClick={this.handleClick.bind(null)} className="dashboard-list">
                <li className='add' onClick={this.handleClick.bind(null)}> <label>Add new Interest </label> </li>
                {InterestList}
              </ul>
            </div>
    );
  }
});

var InterestModule = React.createClass({
  getInitialState: function() {
    return {
      name:'',
      image: '',
      bgcolor: '#000000',
      bimage: "",
      btext: "",
      uploadFor: "interest",
      userData: this.props.userData,
    };
  },
  updateName: function(event)
  {
    var value = event.target.value.split(/[\/\\]/).pop();

    //$("#fileName").val(value);
    var suffix = '';
    var imagePath = this.state.userData.imageLocation + 'interests/';
    if(this.state.uploadFor == 'bounty'){
      suffix = 'bounty/bounty_';
      imagePath += suffix;
      this.state.bimage = imagePath+value;
    }
    else{
      this.state.image=imagePath+value;
    }
    var FILE = "interests/" + suffix + value;
    var POLICY = '{   "expiration": "'+new Date(Date.now()+31536000000).toISOString()+'",  "conditions": [     {"bucket": "' + this.state.userData.BUCKET + '" },     {"acl": "public-read" },     ["eq", "$key", "' + FILE + '"],     ["starts-with", "$Content-Type", "image"],   ] } ';

    var awsid = this.state.userData.AWS_KEY;
    var awskey = this.state.userData.AWS_SECRET;

    var policyText = POLICY.toString();
    var policyBase64 = Base64.encode(policyText);

    var signature = b64_hmac_sha1(awskey, policyBase64);

    this.state.userData['FILE'] = FILE;

    this.state.userData['policy'] = policyBase64;
    this.state.userData['signature'] = signature;

    this.setState(this.state);
  },

  handleSuccess: function(details){
    var partialState = {};
    partialState['name'] = '';
    partialState['image'] = '';
    partialState['bgColor'] = '#000000';
    partialState['bimage'] = '';
    partialState['btext'] = '';
    this.setState(partialState);
    
  },

  handleError: function(xhr, status, err){
    console.error(this.props.url, status, err.toString());
    var partialState = {};
    partialState['message'] = "No";
    this.setState(partialState);
  },

  handleSubmit: function(e) {
    e.preventDefault();
    var name = this.state.name;
    var image = this.state.image;
    var bgColor = this.state.bgcolor;
    var bimage = this.state.bimage;
    var btext = this.state.btext;
    var interestId = this.state.userData.interest;
    /*if (!username || !password) {
      return;
    }*/
    var bountyData = {image: bimage, text: btext};
    var formData = {name:name, image: image, bgColor: bgColor, bounty: bountyData};
    (interestId != '') ? $.extend(formData,{_id: interestId}) : "";
    this.handleLoginSubmit(formData);
    return;
  },

  handleLoginSubmit: function(formData) {
    $.ajax({
      type: 'POST',
      url: this.state.userData.serverURL+this.state.userData.interestApi,
      dataType: 'application/json',
      data: formData,
      success: function(data) {
	     this.handleSuccess(data);
      }.bind(this),
      error: function(xhr, status, err) {
        this.handleError(xhr, status, err);
      }.bind(this)
    });
  },

  handleInputChange: function(key, event) {
    var partialState = {};
    partialState[key] = (event.target.value);
    this.setState(partialState);
  },

  getInterestData: function(){
    var taskId = this.state.userData.interest;
    var taskData = this.state.userData.interestData;
    var task = [];
    taskData.map(function(tempTask){
      if(tempTask._id == taskId){
        task = tempTask;
      }
    });
    return task;
  },

  submitForm: function(event){
    event.preventDefault();
    $.ajax({
      url: this.state.userData.S3UploadURL,
      type: 'POST',
      data: new FormData($('#uploadForm')[0]),
      async: false,
      cache: false,
      contentType: false,
      processData: false,
      success: function () {
          //alert('Form Submitted!');
      },
      error: function(err){
        //console.log(err);
          //alert("error in ajax form submission");
      }
    });
  },

  componentWillMount: function() {
    var partialState = {};
    var interestId = this.state.userData.interest;
    var userData = this.state.userData;
    var interest = (interestId != undefined && interestId != "") ? this.getInterestData() : '';
    partialState['name'] = (interestId != undefined && interestId != "") ? interest.name : this.state.name;
    partialState['image'] = (interestId != undefined && interestId != "") ? interest.image : this.state.image;
    partialState['bgcolor'] = (interestId != undefined && interestId != "") ? interest.bgColor : this.state.bgcolor;
    partialState['bimage'] = (interestId != undefined && interestId != "") ? interest.bounty.image : this.state.bimage;
    partialState['btext'] = (interestId != undefined && interestId != "") ? interest.btext : this.state.btext;
    this.setState(partialState);
  },

  setDropDown: function(event){
    this.setState({'uploadFor' : $(event.target).text()});
  },

  render: function() {
    var name = this.state.name;
    var image = this.state.image;
    var bgcolor = this.state.bgcolor;
    var bimage = this.state.bimage;
    var btext = this.state.btext;
    var fileName = this.state.userData.FILE;
    var AWSAccessKeyId = this.state.userData.AWS_KEY;
    var policy = this.state.userData.policy;
    var uploadFor = this.state.uploadFor;
    var signature = this.state.userData.signature;
    var showImage = "";
    if(image != ''){
      showImage = <img className="dashboard-avatar" src={image}/>;
    }
    var showbImage = "";
    if(bimage != ''){
      showbImage = <img className="dashboard-avatar" src={bimage}/>;
    }
    return (<div>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label for="name">Name: </label>
              <input type="text" className="form-control" id="name" placeholder="Enter Name" value={name} onChange={this.handleInputChange.bind(null, 'name')} />
            </div>
            <div className="form-group">
              <label for="bgcolor">Background Color: </label>
              <input type="text" className="form-control" id="bgcolor" placeholder="#000000" value={bgcolor} onChange={this.handleInputChange.bind(null, 'bgcolor')} />
            </div>
            <div className="form-group">
              <label for="image">Interest Image: </label>
              <input type="text" className="form-control" id="image" placeholder="Enter Image Path" value={image} onChange={this.handleInputChange.bind(null, 'image')} />
              {showImage}
            </div>
            <br />  
            <br />
            <div className="form-group">
              <label for="bimage">Bounty Image: </label>
              <input type="text" className="form-control" id="bimage" placeholder="Enter Image Path" value={bimage} onChange={this.handleInputChange.bind(null, 'bimage')} />
              {showbImage}
            </div>
            <br />
            <br />
            <div className="form-group">
              <label for="btextr">Bounty Description: </label>
              <textarea className="form-control" id="btext" placeholder="" value={btext} onChange={this.handleInputChange.bind(null, 'btext')} ></textarea>
            </div>
            <button type="submit" className="btn btn-default">Submit</button>
          </form>
          <div id='uploadImageDiv'>
            <div className="form-group">
              <label for="uploadFor">Upload Image for : </label>
              <select className="form-control" value={uploadFor} onChange={this.handleInputChange.bind(null, 'uploadFor')} >
                <option value=''> Please Select </option>
                <option value='interest'> Interest </option>
                <option value='bounty'> Bounty </option>
              </select>
            </div>
            <form id="uploadForm" action="javascript:;" method="post" encType="multipart/form-data" onSubmit={this.submitForm}>
              <input type="hidden" id="fileName" name="key" value={fileName} />
              <input type="hidden" name="acl" value="public-read" />
              <input type="hidden" name="content-type" value="image/png" />
              <input type="hidden" name="AWSAccessKeyId" value={AWSAccessKeyId} />
              <input type="hidden" id="policy" name="policy" value={policy} />
              <input type="hidden" id="signature" name="signature" value={signature} />
              <input id="file" name="file" type="file" onChange={this.updateName.bind(null)} accept="image/*"/>
              <input name="submit" className='btn btn-success btn-sm' value="Upload" type="submit" onSubmit={this.submitForm}/>
            </form>
          </div>
        </div>
    );
  }
});
