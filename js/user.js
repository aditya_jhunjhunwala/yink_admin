var UserModule = React.createClass({
  getInitialState: function() {
    return {
      userData: this.props.userData,
      interestData: [],
    };
  },
  
  handleClick: function(e){
    React.unmountComponentAtNode(document.getElementById('contentArea'));
    var target = $(e.target).closest('li');
    if($(e.target).hasClass('edit')){
      this.state.userData['selectedUserId'] = target.attr('id');
      this.setState(this.state.userData);
      React.render(
        <UserModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else if($(e.target).hasClass('add')){
      this.state.userData['selectedUserId'] = '';
      this.setState(this.state.userData);
      React.render(
        <UserModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else{  
    this.state.userData['selectedUserId'] = target.attr('id');
    this.setState(this.state.userData);
    React.render(
      <UserHistoryModule userData={this.state.userData}/>,
      document.getElementById('listArea')
      );
    }
  },

  render: function() {
    var data = this.state.userData;
    var userList = data.userListData === undefined ? '' : data.userListData.map(function (user) {
      return(
        <li id={user._id}>
          <a href="#">
          <img className="dashboard-avatar" alt={user.name}
              src={user.image} /></a>
          <strong>Name:</strong> <a href="#">{user.name}
          </a><br /><br /><br />
        </li>
        /*<li id={user._id}>{user.name}</li>*/

      );
    });
    return (
      <div className="box-content">
        <ul onClick={this.handleClick.bind(null)} className="dashboard-list">
          {userList}
        </ul>
      </div>
    );
  }
});

var UserHistoryModule = React.createClass({
  getInitialState: function() {
    return {
      limit:'20',
      type: 'recent',
      answer_status: 'verified',
      reason: "",
      userId: "",
      userData: this.props.userData,
      interestData: [],
    };
  },

  handleClick: function(e){
    var target = $(e.target).closest('li');
    if($(e.target).hasClass('edit')){
      this.state.userData['historyId'] = target.attr('id');
      this.setState(this.state.userData);
      React.render(
        <InterestModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else if($(e.target).hasClass('add')){
      this.state.userData['historyId'] = '';
      this.setState(this.state.userData);
      React.render(
        <InterestModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else{ 
    this.state.userData['historyId'] = target.attr('id');
    this.setState(this.state.userData);
    React.render(
      <UserTaskModule userData={this.state.userData}/>,
      document.getElementById('contentArea')
    );
  }
  },

  getDataFromServer: function(){
    var formData = {
      //access_token: this.state.userData.token,
      userId: this.state.userData.selectedUserId,
      detail: 1,
    }
    $.ajax({
      url: this.state.userData.serverURL+this.state.userData.userHistoryApi,
      type: 'GET',
      data: formData,
      dataType: 'json',
      async: false,
      success: function(data) {
      	delete this.state.userData['userHistoryData'];
        this.state.userData['userHistoryData'] = data;
        this.setState(this.state.userData);
      }.bind(this),
      error: function(xhr, status, err) {
        delete this.state.userData['userHistoryData'];
        this.setState(this.state.userData);
      }.bind(this)
    });
  },

  componentWillMount: function() {
      //if(this.state.userData.userHistoryData === undefined)
        this.getDataFromServer();
  },

  handlePageClick: function(key){
    var data = {};
    if(this.state.userData.communityData !== undefined && key == 'prev') { 
      data['previous'] = this.state.userData.communityData.previous;
    }
    else if(this.state.userData.communityData !== undefined && key == 'next'){
      data['next'] = this.state.userData.communityData.next;
    }
    this.getDataFromServer(data);
  },

  render: function() {
    var limit = this.state.limit;
    var type = this.state.type;
    var taskId = this.state.userData.task;
    var answer_status = this.state.answer_status;
    var needVerify = false;
    var reason = this.state.reason;
    var data = this.state.userData;
    var HistoryList = data.userHistoryData === undefined ? '' : data.userHistoryData.map(function (history) {
      var task =  <li id={history._id}>
                  <div>
                    <span>
                      {history.title}
                    </span>
                    <br /><br />
                    <span>
                      Rewards:<br />
                        Type of Reward: <em> {history.rewards.reward_type} </em><br />
                        Reward Value: <em> {history.rewards.value} </em> <br />
                    </span>
                    
                  </div>
                  </li>;
      return({task});
    }.bind(this));
    return (
      <div className="box-content userHistory">
        <ul onClick={this.handleClick.bind(null)} className="dashboard-list">
          {HistoryList}
        </ul>
      </div>
    );
  }
});


var UserTaskModule = React.createClass({
  getInitialState: function() {
    return {
      message: "",
      userData: this.props.userData,
    };
  },

  render: function(){
    var HistoryID = this.state.userData.historyId;
    var TaskList = this.state.userData.userHistoryData === undefined ? '' : this.state.userData.userHistoryData.map(function (history) {

      var response = "";
        if(history._id == HistoryID){
          var response = history.responses.map(function(response){
          var answer = response.answers[0];

          if(answer.response.attachment[0].type == "video"){
            answer_frame = 
              <video width="320" height="240" controls autoplay loop>
                <source src={answer.response.attachment[0].streaming_url} />
                <source src={answer.response.attachment[0].url} />
                Your browser does not support the video tag.
              </video> 
          }
          else if(answer.response.attachment[0].type == 'link'){
            answer_frame = <iframe src={answer.response.attachment[0].url}></iframe>
          }
          else{
            answer_frame = <img src={answer.response.attachment[0].url} />
          }
          var my_answer =  <div id={answer.answer_id}> 
                      {answer.response.text} 
                      <br /><br />
                      {answer_frame}
                    </div> ;
          return(
              <li id={answer._id}>     
                <b>Answer:</b> {my_answer}<br />             
                <b>Flags:</b> {answer.yflag.length} &nbsp;&nbsp;
                <b>Shares:</b> {answer.yshare.length} <br />
                <b>Views:</b> {answer.yview.length} &nbsp;&nbsp;
                <b>Likes:</b> {answer.yo.length}<br /> 
              </li>
            );
          });
        }
        return ({response});
    });

    return(
      <div id='userTaskList'>
      <ul>
        {TaskList}
      </ul>
        </div>
      );
    
  },
});
