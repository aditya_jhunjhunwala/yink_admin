var LibraryListModule = React.createClass({
  getInitialState: function() {
    return {
      userData: this.props.userData,
      marker: "",
      interest: "",
      id: "",
      interestData: [],
    };
  },
  handleClick: function(e){
    React.unmountComponentAtNode(document.getElementById('contentArea'));
    var target = $(e.target).closest('li');
    if($(e.target).hasClass('edit')){
      this.state.userData['interest'] = target.attr('id');
      this.setState(this.state.userData);
      React.render(
        <InterestModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else if($(e.target).hasClass('add')){
      this.state.userData['interest'] = '';
      this.setState(this.state.userData);
      React.render(
        <InterestModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else{ 
      this.state.userData['interest'] = target.attr('id');
      this.setState(this.state.userData);
      React.render(
        <CampaignListModule userData={this.state.userData}/>,
        document.getElementById('listArea')
      );
    }
  },
  getDataFromServer: function(key, api, marker){
    var formData = {}
    if(key == 'uploadImageList')
    {
      formData = {
        Marker: marker,
      }
    }

    var url = this.state.userData.serverURL + api;
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      data: formData,
      async: false,
      success: function(data) {
        this.state.userData[key] = data;
        this.setState(this.state.userData);
      }.bind(this),
      error: function(xhr, status, err) {
        /*console.error(this.props.url, status, err.toString());*/
      }.bind(this)
    });
  },

  handlePageClick: function(key, marker){
    var data = {};
    if(this.state.userData.uploadImageList !== undefined && key == 'prev') { 
      data['Marker'] = marker;
    }
    else if(this.state.userData.uploadImageList !== undefined && key == 'next'){
      data['Marker'] = marker;
      this.state['marker'] = this.state.userData.uploadImageList.Marker;
      this.setState(this.state);   
    }
    this.getDataFromServer('uploadImageList', this.state.userData.imageApi, marker);
  },


  addToLibrary: function(url, id, selectedInterest){
    var formData = {
      url: url,
      type: 'image',
      categories: selectedInterest,
    };
    console.log(formData);
    $.ajax({
      type: 'POST',
      url: this.state.userData.serverURL+this.state.userData.addImageApi,
      dataType: 'application/json',
      data: formData,
      success: function(data) {
        /*this.handleSuccess(data);*/
      }.bind(this),
      error: function(xhr, status, err) {
        /*this.handleError(xhr, status, err);*/
      }.bind(this)
    });
  },

  handleChange:function(id, e){
    var partialState = {};
    partialState[id] = e.target.value;
    this.setState({interest: partialState});
    console.log(this.state);
    //this.setState({interest:e.target.value, id: id} );
  },

  render: function() {
    var data = this.state.userData;
    var marker = this.state.marker;
    var checked = "";
    var count = 0;
    var interest = this.state.interest;
    var id = this.state.id;
    var primaryInterest = "";
    var InterestNodes = this.state.userData.interestData === undefined ? '' : this.state.userData.interestData.map(function (interest) {
      if(primaryInterest == "") primaryInterest = interest._id;
      return (
        <option value={interest._id}>
          {interest.name}
        </option>
      );
    });

    var ImageList = data.uploadImageList === undefined ? '' : data.uploadImageList.Contents.map(function (image) {
      
      if(image.Key.match(/(.jpg|.png|.jpeg|.gif)/) && count < 15){
        var imageURL = data.imageLocation+image.Key;
        marker = image.Key;
        count++;
        var selectedInterest = primaryInterest;
        if(interest.ETag){
          selectedInterest = interest.ETag;
        }

        return(
          <li id={image.ETag}>
            <img className="dashboard-avatar" src={imageURL} />
            <input type='hidden' value={imageURL} />
            
            <select name="iname" value={selectedInterest} onChange={this.handleChange.bind(null,image.ETag)}>
              {InterestNodes}
            </select>
            <input type='button' className='pull-right btn btn-success btn-xs' onClick={this.addToLibrary.bind(null, imageURL, image.ETag, selectedInterest)} value='Add to Library' />
            <br /><br />
          </li> 
        );
      }
    }.bind(this));

    var nextLink = "";
    var prevLink = "";
    
    if(data.uploadImageList !== undefined)
      nextLink = <span className='next' onClick={this.handlePageClick.bind(null, 'next',marker)}>Next</span>;
    if(data.uploadImageList !== undefined && data.uploadImageList.Marker != "")
      prevLink = <span className='previous'onClick={this.handlePageClick.bind(null, 'prev',this.state.marker)}>Previous</span>;

    return (
      <div className="box-content" id="uploadImageList">
        <ul className="pager"><li className="previous">{prevLink}</li><li className="next">{nextLink}</li></ul>
        <ul className="dashboard-list">
          {ImageList}
        </ul>
      </div>
    );
  }
});
