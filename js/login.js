/* 
 * This module is used to handle the login functionality. 
 * This takes Username and password as input and returns the admin management page to the user.
 */
var LoginModule = React.createClass({
  /* 
   * This acts as a constructor for the module and initializes the values of the state variables.
   */
  getInitialState: function() {
    return {
      email: 'always.akshat@gmail.com',
      password: 'iamadmin',
      message: '',
      userData: this.props.userData,
    };
  },

  /*
   * This is used to update the value of the controlled HTML component
  */
  handleInputChange: function(key, event) {
    var partialState = {};
    partialState[key] = (event.target.value);
    this.setState(partialState);
  },

  /*
   * Verifies the input and prepares the data to be posted to the server.
   */
  handleSubmit: function(e) {
    e.preventDefault();
    var username = this.state.email;
    var password = this.state.password;
    if (!username || !password) {
      return;
    }
    var formData = {email: username, password: password};
    this.handleFormSubmit(formData);
    return;
  },

  /*
   * Performs the action when AJAX request returns success.
   */
  handleSuccess: function(userData){
    this.checkUserType(userData);
  },

  /*
   * Determines the logged in user's role and displays the appropriate interface.
   */
  checkUserType: function(userData) {
    var partialState = {};
    console.log(userData);
    if(userData.role != "admin"){
      partialState['message'] = "Hello User";
      //TODO START - To be moved to role == 'admin'
      userData['url'] = this.state.url;
      $.extend(userData, this.state.userData);
      console.log(userData);
      React.render(
        <AdminModule userData={userData} />,
        document.getElementById('container')
      );
      //TODO END
    }
    else{
      partialState['message'] = "Hello Admin";
    }
    var partialState = {};
    this.setState(partialState);
  },
  /*
   * Performs the action when AJAX request returns failure.
   * Displays an error message on the screen.
   */
  handleError: function(xhr, status, err){
    var partialState = {};
    partialState['message'] = "Something Went Wrong.";
    this.setState(partialState);
  },

  /*
   * Submits the form to the server.
   * 
   */
  handleFormSubmit: function(formData) {
    var userData = this.state.userData;
    $.ajax({
      type: 'POST',
      url: userData.serverURL+userData.loginApi,
      dataType: 'json',
      data: formData,
      success: function(data) {
	       this.handleSuccess(data);
      }.bind(this),
      error: function(xhr, status, err) {
        this.handleError(xhr, status, err);
      }.bind(this)
    });
  },

  /*
   * Renders the HTML page.
   * This is called on every state update.
   */
  render: function() {
    var email = this.state.email;
    var password = this.state.password;
    return (
      <div className="row">
        <div className="well col-md-5 center login-box">
            <div className="alert alert-info">
                Please login with your Username and Password.
            </div>
            <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <fieldset>
                    <div className="input-group input-group-lg">
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user red"></i></span>
                        <input type="text" className="form-control" placeholder="Username" value={email} onChange={this.handleInputChange.bind(null, 'email')}/>
                    </div>
                    <div className="clearfix"></div><br />

                    <div className="input-group input-group-lg">
                        <span className="input-group-addon"><i className="glyphicon glyphicon-lock red"></i></span>
                        <input type="password" className="form-control" placeholder="Password" value={password} onChange={this.handleInputChange.bind(null, 'password')} />
                    </div>
                    <div className="clearfix"></div>

{/*                    <div className="input-prepend">
                        <label className="remember" for="remember"><input type="checkbox" id="remember" /> Remember me</label>
                    </div>
                    <div className="clearfix"></div>
*/}
                    <p className="center col-md-5">
                        <button type="submit" className="btn btn-primary">Login</button>
                    </p>
                </fieldset>
            </form>
        </div>
        
    </div>
    );
  }
});

/* 
 * Module to display appropriate message to the user during login.
 */
var MessageModule = React.createClass({
  /*
   * Renders the HTML page.
   * This is called on every state update.
   */
  render: function(){
    return(<span>{this.props.message} </span>);
  },
});

