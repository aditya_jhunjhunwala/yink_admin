var TaskListModule = React.createClass({
  getInitialState: function() {
    return {
      userData: this.props.userData,
      interestData: [],
    };
  },

  handleClick: function(e){
    React.unmountComponentAtNode(document.getElementById('contentArea'));
    var target = $(e.target).closest('li');
    if($(e.target).hasClass('edit')){
      var partialState = {};
      this.state.userData['task'] = target.attr('id');
      this.setState(this.state.userData);
      React.render(
        <TaskModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else if($(e.target).hasClass('add')){
      var partialState = {};
      this.state.userData['task'] = '';
      this.setState(this.state.userData);
      React.render(
        <TaskModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else if($(e.target).hasClass('delete')){
      this.deleteFromServer(target.attr('id'));
      React.render(
        <TaskModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else{  
      var partialState = {};
      this.state.userData['task'] = target.attr('id');
      this.setState(this.state.userData);
      React.render(
        <CommunityListModule userData={this.state.userData}/>,
        document.getElementById('listArea')
      );
    }
  },

  deleteFromServer: function(id){
    $.ajax({
      type: 'GET',
      url: this.state.userData.serverURL+this.state.userData.taskApi+id+this.state.userData.deleteApi,
      dataType: 'application/json',
      success: function(data) {
        this.getDataFromServer('taskData', this.state.userData.taskApi);
      }.bind(this),
      error: function(xhr, status, err) {
        this.getDataFromServer('taskData', this.state.userData.taskApi);
      }.bind(this)
    });
  },

  getDataFromServer: function(key, api){
    var url = this.state.userData.serverURL + api;
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      async: false,
      success: function(data) {
        this.state.userData[key] = data;
        this.setState(this.state.userData);   
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  render: function() {
    var data = this.state.userData;

    var TaskList = data.taskData === undefined ? '' : data.taskData.map(function (task) {
      var editSpan = <button className=' btn btn-primary btn-xs edit'>Edit </button> ;
      var deleteSpan = <button className='delete btn btn-danger btn-xs'>Delete </button>;
      if(task.campaign_id == data.campaign){
      return(
        /*<li id={task._id}><div className='leftClass'><img src={task.image} width="50" height="50" />{task.title}</div><div className='rightClass'>{editSpan}{deleteSpan}</div></li>*/
        <li id={task._id}>
            <a href="#">
            <img className="dashboard-avatar" alt={task.title}
                src={task.image} /></a>
            <strong>Name:</strong> <a href="#">{task.title}
            </a><br />
            {editSpan} &nbsp; &nbsp;
            {deleteSpan}
            <br />
          </li>
      );
    }
    });
    return (
      <div className="box-content">
        <ul onClick={this.handleClick.bind(null)} className="dashboard-list">
          <li className='add' onClick={this.handleClick.bind(null)}> <label> Add new Task </label> </li>
          {TaskList}
        </ul>
      </div>
    );
  }
});

var TaskModule = React.createClass({
  getInitialState: function() {
    return {
      name            : '',
      title           : '',
      description     : '',
      image           : '',
      rvalue          : '0',
      rtype           : '',
      verify          : 'false',
      parent          : null,
      showRep         : '0',
      showGen         : '',
      showEmail       : '',
      unlockRep       : '0',
      leaderboard     : '1',
      community       : '1',
      attempt         : '10',
      actType         : 'content-creation',
      active          : 'active',
      uploadFor: 'tasks',
      activity : '',
      atype : 'text',
      campaign: null,
      userData: this.props.userData,
      taskData: [],
      campaignData: [],
    };
  },

  getDataFromServer: function(dataField){
    //dataField = campaignID == "" ? dataField : "campaign/" + campaignID;
    $.ajax({
      url: this.state.url+ '/api/' + dataField,
      type: 'GET',
      dataType: 'json',
      success: function(data) {
        var fieldtoSet = dataField == "campaign" ? "campaignData" : "taskData";
        var partialState = {};
        partialState[fieldtoSet] = data;
        this.setState(partialState);   
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  handleSubmit: function(e) {
    e.preventDefault();
    var formData = [];
    var taskId = this.state.userData.task;
    var name = this.state.name;
    var title = this.state.title;
    var description = this.state.description;
    var image = this.state.image;
    var rvalue = this.state.rvalue;
    var rtype = this.state.rtype;
    var verify = this.state.verify;
    var parent = this.state.parent;
    var showRep = this.state.showRep;
    var showGen = this.state.showGen;
    var showEmail = this.state.showEmail;
    var unlockRep = this.state.unlockRep;
    var leaderboard = this.state.leaderboard;
    var community = this.state.community;
    var attempt = this.state.attempt;
    var actType = this.state.actType;
    var active = this.state.active;
    var activity = this.state.activity;
    var atype = this.state.atype;
    var campaign = this.state.userData.campaign;

    var rewardsData = {};
    (rvalue != '') ? $.extend(rewardsData,{value: rvalue}) : "";
    (rtype != '') ? $.extend(rewardsData,{type: rtype}) : "";
    var showData = {};
    (showGen != '') ? $.extend(showData,{gender: showGen}) : "";
    (showRep != '') ? $.extend(showData,{reputation: showRep}) : "";
    (showEmail != '') ? $.extend(showData,{email: showEmail}) : "";
    var unlockData = {reputation: unlockRep};
    var conditionData = {show: showData, unlock: unlockData, leaderboard:leaderboard, community: community, attempt:attempt};
    var activitiesData = [{activity: activity, type: atype}];
    var formData = {campaign_id:campaign, title: title, description: description, image: image, rewards: rewardsData, verify: verify, condition: conditionData, type: actType, active: active, activities: activitiesData};
    (parent != null) ? $.extend(formData,{parent: parent}) : ""; 
    (taskId != '') ? $.extend(formData,{_id: taskId}) : "";
    this.handleLoginSubmit(formData);

    return;
  },

  handleLoginSubmit: function(formData) {
    $.ajax({
      type: 'POST',
      url: this.state.userData.serverURL+this.state.userData.taskApi,
      dataType: 'application/json',
      data: formData,
      success: function(data) {
        this.getDataFromServer('task');
      }.bind(this),
      error: function(xhr, status, err) {
        this.getDataFromServer('task');
      }.bind(this)
    });
  },

  handleInputChange: function(key, event) { 
    var partialState = {};
    partialState[key] = (event.target.value);
    this.setState(partialState);
  },

  componentWillMount: function() {
    var partialState = {};
    var taskId = this.state.userData.task;
    var userData = this.state.userData;
    var task = (taskId != undefined && taskId != "") ? this.getTaskData() : '';
    var name = this.state.name;
    partialState['title'] = (taskId != undefined && taskId != "") ? task.title :this.state.title;
    partialState['description'] = (taskId != undefined && taskId != "") ? task.description : this.state.description;
    partialState['image'] = (taskId != undefined && taskId != "") ? task.image : this.state.image;
    partialState['rvalue'] = (taskId != undefined && taskId != "") ? task.rewards.value : this.state.rvalue;
    partialState['rtype'] = (taskId != undefined && taskId != "") ? task.rewards.type : this.state.rtype;
    partialState['verify'] = (taskId != undefined && taskId != "") ? task.verify : this.state.verify;
    partialState['parent'] = (taskId != undefined && taskId != "") ? task.parent : this.state.parent;
    partialState['showRep'] = (taskId != undefined && taskId != "" && task.condition.show != undefined) ? task.condition.show.reputation : this.state.showRep;
    partialState['showGen'] = (taskId != undefined && taskId != "" && task.condition.show != undefined) ? task.condition.show.gender : this.state.showGen;
    partialState['showEmail'] = (taskId != undefined && taskId != "" && task.condition.show != undefined) ? task.condition.show.email : this.state.showEmail;
    partialState['unlockRep'] = (taskId != undefined && taskId != "") ? task.condition.unlock.reputation : this.state.unlockRep;
    partialState['leaderboard'] = (taskId != undefined && taskId != "") ? task.condition.leaderboard : this.state.leaderboard;
    partialState['community'] = (taskId != undefined && taskId != "") ? task.condition.community : this.state.community;
    partialState['attempt'] = (taskId != undefined && taskId != "") ? task.condition.attempt : this.state.attempt;
    partialState['actType'] = (taskId != undefined && taskId != "") ? task.type : this.state.actType;
    partialState['active'] = (taskId != undefined && taskId != "") ? task.active : this.state.active;
    partialState['activity'] = (taskId != undefined && taskId != "") ? task.activities[0].activity : this.state.activity;
    partialState['atype'] =  this.state.atype;
    partialState['campaign'] = (taskId != undefined && taskId != "") ? userData.campaign : this.state.campaign;
    this.setState(partialState);
  },

  handleChange:function(key, e){
    if(key == "campaign"){
     // this.getParentTasks(e.target.value);
    }
    var partialState = {};
    partialState[key] = (e.target.value);
    this.setState(partialState);
  },

  getParentTasks: function(campaignID){
    var campaign = this.state.campaign;
    this.getDataFromServer('task',campaignID);

  },

  getTaskData: function(){
    var taskId = this.state.userData.task;
    var taskData = this.state.userData.taskData;
    var task = [];
    taskData.map(function(tempTask){
      if(tempTask._id == taskId){
        task = tempTask;
      }
    });
    return task;
  },

    submitForm: function(event){
    event.preventDefault();
    $.ajax({
      url: this.state.userData.S3UploadURL,
      type: 'POST',
      data: new FormData($('#uploadForm')[0]),
      async: false,
      cache: false,
      contentType: false,
      processData: false,
      success: function () {
          //alert('Form Submitted!');
      },
      error: function(err){
        //console.log(err);
          //alert("error in ajax form submission");
      }
    });
  },

  updateName: function(event)
  {
    var value = event.target.value.split(/[\/\\]/).pop();
    //$("#fileName").val(value);
    var suffix = '';
    var imagePath = this.state.userData.imageLocation + 'tasks/';
    if(this.state.uploadFor == 'bounty'){
      suffix = 'bounty/bounty_';
      imagePath += suffix;
      this.state.bimage = imagePath+value;
    }
    else{
      this.state.image=imagePath+value;
    }
    var FILE = "tasks/" + suffix + value;
    var POLICY = '{   "expiration": "'+new Date(Date.now()+31536000000).toISOString()+'",  "conditions": [     {"bucket": "' + this.state.userData.BUCKET + '" },     {"acl": "public-read" },     ["eq", "$key", "' + FILE + '"],     ["starts-with", "$Content-Type", "image"],   ] } ';

    var awsid = this.state.userData.AWS_KEY;
    var awskey = this.state.userData.AWS_SECRET;

    var policyText = POLICY.toString();
    var policyBase64 = Base64.encode(policyText);

    var signature = b64_hmac_sha1(awskey, policyBase64);

    this.state.userData['FILE'] = FILE;

    this.state.userData['policy'] = policyBase64;
    this.state.userData['signature'] = signature;

    this.setState(this.state);
  },

  render: function() {
    var taskId = this.state.userData.task;
    var userData = this.state.userData;
    var task = (taskId != undefined && taskId != "") ? this.getTaskData() : '';
    var title = this.state.title;
    var description = this.state.description;
    var image = this.state.image;
    var rvalue = this.state.rvalue;
    var rtype = this.state.rtype;
    var verify = this.state.verify;
    var parent = this.state.parent;
    var showRep = this.state.showRep;
    var showGen = this.state.showGen;
    var showEmail = this.state.showEmail;
    var unlockRep = this.state.unlockRep;
    var leaderboard = this.state.leaderboard;
    var community = this.state.community;
    var attempt = this.state.attempt;
    var actType = this.state.actType;
    var active = this.state.active;
    var activity = this.state.activity;
    var atype = this.state.atype;
    var campaign = this.state.userData.campaign;    
    var campaignData = userData.campaignData;
    var taskData = userData.taskData;
    var fileName = this.state.userData.FILE;
    var AWSAccessKeyId = this.state.userData.AWS_KEY;
    var policy = this.state.userData.policy;
    var uploadFor = this.state.uploadFor;
    var signature = this.state.userData.signature;
    

    var CampaignNodes = campaignData === undefined ? '' : campaignData.map(function (campaign) {

      return (

        <option value={campaign._id}>
          {campaign.name}
        </option>
      );
    });
    var ParentTaskNodes = taskData === undefined ? '' : taskData.map(function (task) {

      if(task.campaign_id == campaign && task._id != taskId){
      return (
        <option value={task._id}>
          {task.title}
        </option>
      );
      }
    });
    var showImage = "";
    if(image != ''){
      showImage = <div> <img src={image} width="50" height="50"/></div>;
    }
    /*<li>  
                Campaign: <select value={campaign} onChange={this.handleChange.bind(null, 'campaign')} >
                    <option> Please Select</option>
                    {CampaignNodes}
                  </select>
              </li>*/
    return (
      <div>
        <p>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label for="title">Title </label>
              <input type="text" className="form-control" id="title" placeholder="Title" value={title} onChange={this.handleInputChange.bind(null, 'title')} />
            </div>
            <div className="form-group">
              <label for="description">Description </label>
              <input type="text" className="form-control" id="description" placeholder="Description" value={description} onChange={this.handleInputChange.bind(null, 'description')} />
            </div>
            <div className="form-group">
              <label for="image">Image </label>
              <input type="text" className="form-control" id="image" placeholder="Image Path" value={image} onChange={this.handleInputChange.bind(null, 'image')} />
              {showImage}
            </div>
            <div className="form-group">
              <label for="rvalue">Rewards Value </label>
              <input type="text" className="form-control" id="rvalue" placeholder="0" value={rvalue} onChange={this.handleInputChange.bind(null, 'rvalue')} />
            </div>
            <div className="form-group">
              <label for="rtype">Rewards Type </label>
              <select className="form-control" value={rtype} onChange={this.handleChange.bind(null, 'rtype')} >
                <option value='coupon'> Coupon </option>
                <option value='points'> Points </option>
                <option value='bounty'> Bounty </option>
              </select>
            </div>
            <div className="form-group">
              <label for="verify">Verify </label>
              <select className="form-control" value={verify} onChange={this.handleChange.bind(null, 'verify')} >
                <option value='false'> No </option>
                <option value='true'> Yes </option>
              </select>
            </div>
            <div className="form-group">
              <label for="parent">Parent Task </label>
              <select className="form-control" value={parent} onChange={this.handleChange.bind(null, 'parent')} >
                <option> Please Select</option>
                {ParentTaskNodes}
              </select>
            </div>
            <div className="form-group">
              <label for="showRep">Minimum Reputation to Show </label>
              <input type="text" className="form-control" id="showRep" placeholder="0" value={showRep} onChange={this.handleInputChange.bind(null, 'showRep')} />
            </div>
            <div className="form-group">
              <label for="showGen">Gender to Show </label>
              <select className="form-control" value={showGen} onChange={this.handleChange.bind(null, 'showGen')} >
                <option value=''> Please Select </option>
                <option value='male'> Male </option>
                <option value='female'> Female </option>
                <option value='others'> Others </option>
              </select>
            </div>
            <div className="form-group">
              <label for="showEmail">Emails to Show </label>
              <input type="text" className="form-control" id="showEmail" placeholder="john@doe.com" value={showEmail} onChange={this.handleInputChange.bind(null, 'showEmail')} />
            </div>
            <div className="form-group">
              <label for="unlockRep">Unlock at Reputation </label>
              <input type="text" className="form-control" id="unlockRep" placeholder="0" value={unlockRep} onChange={this.handleInputChange.bind(null, 'unlockRep')} />
            </div>
            <div className="form-group">
              <label for="leaderboard">Show Leaderboard</label>
              <select className="form-control" value={leaderboard} onChange={this.handleChange.bind(null, 'leaderboard')} >
                <option value='0'> No </option>
                <option value='1'> Yes </option>
              </select>
            </div>
            <div className="form-group">
              <label for="community">Show Community</label>
              <select className="form-control" value={community} onChange={this.handleChange.bind(null, 'community')} >
                <option value='0'> No </option>
                <option value='1'> Yes </option>
              </select>
            </div>
            <div className="form-group">
              <label for="attempt">Attempts Allowed </label>
              <input type="text" className="form-control" id="attempt" placeholder="0" value={attempt} onChange={this.handleInputChange.bind(null, 'attempt')} />
            </div>
            <div className="form-group">
              <label for="actType">Task Type</label>
              <select className="form-control" value={actType} onChange={this.handleChange.bind(null, 'actType')} >
                <option value='content-creation'> Content Creation </option>
                <option value='topic-creation'> Topic Creation </option>
                <option value='brand-task'> Brand Task </option>
              </select>
            </div>
            <div className="form-group">
              <label for="active">Active</label>
              <select className="form-control" value={active} onChange={this.handleChange.bind(null, 'active')} >
                <option value='active'> Active </option>
                <option value='expired'> Expired </option>
              </select>
            </div>
            <div className="form-group">
              <label for="atype">Activity Type</label>
              <select className="form-control" value={atype} onChange={this.handleChange.bind(null, 'atype')} >
                <option value='text'> Text </option>
              </select>
            </div>
            <div className="form-group">
              <label for="activity">Activity Details </label>
              <input type="text" className="form-control" id="activity" placeholder="Activity" value={activity} onChange={this.handleInputChange.bind(null, 'activity')} />
            </div>
            <label>
              <input className="btn btn-default" type="submit" value='Add'/>
            </label>
          </form>

          <div id='uploadImageDiv'>
          <label> Upload Image: </label>
          <form id="uploadForm" action="javascript:;" method="post" encType="multipart/form-data" onSubmit={this.submitForm}>
            <input type="hidden" id="fileName" name="key" value={fileName} />
            <input type="hidden" name="acl" value="public-read" />
            <input type="hidden" name="content-type" value="image/png" />
            <input type="hidden" name="AWSAccessKeyId" value={AWSAccessKeyId} />
            <input type="hidden" id="policy" name="policy" value={policy} />
            <input type="hidden" id="signature" name="signature" value={signature} />
            <input id="file" name="file" type="file" onChange={this.updateName.bind(null)} accept="image/*"/>
            <input name="submit" className='btn btn-success btn-sm' value="Upload" type="submit" onSubmit={this.submitForm}/>
          </form>
          </div>

        </p>
      </div>
    );
  }
});


