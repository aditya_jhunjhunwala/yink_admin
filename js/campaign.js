var CampaignListModule = React.createClass({
  getInitialState: function() {
    return {
      userData: this.props.userData,
      interestData: [],
    };
  },
  
  handleClick: function(e){
    React.unmountComponentAtNode(document.getElementById('contentArea'));
    var target = $(e.target).closest('li');
    if($(e.target).hasClass('edit')){
      this.state.userData['campaign'] = target.attr('id');
      this.setState(this.state.userData);
      React.render(
        <CampaignModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else if($(e.target).hasClass('add')){
      this.state.userData['campaign'] = '';
      this.setState(this.state.userData);
      React.render(
        <CampaignModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else if($(e.target).hasClass('delete')){
      this.deleteFromServer(target.attr('id'));
      React.render(
        <CampaignModule userData={this.state.userData}/>,
        document.getElementById('contentArea')
      );
    }
    else{  
    this.state.userData['campaign'] = target.attr('id');
    this.setState(this.state.userData);
    React.render(
      <TaskListModule userData={this.state.userData}/>,
      document.getElementById('listArea')
      );
    }
  },

  deleteFromServer: function(id){
    $.ajax({
      type: 'GET',
      url: this.state.userData.serverURL+this.state.userData.campaignApi+'/'+id+this.state.userData.deleteApi,
      dataType: 'application/json',
      success: function(data) {
        this.getDataFromServer('campaignData', this.state.userData.campaignApi);
      }.bind(this),
      error: function(xhr, status, err) {
        this.getDataFromServer('campaignData', this.state.userData.campaignApi);
      }.bind(this)
    });
  },

  getDataFromServer: function(key, api){
    var url = this.state.userData.serverURL + api;
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      async: false,
      success: function(data) {
        this.state.userData[key] = data;
        this.setState(this.state.userData);   
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  render: function() {
    var data = this.state.userData;
    var campaignList = data.campaignData === undefined ? '' : data.campaignData.map(function (campaign) {
      var editSpan = <button className=' btn btn-primary btn-xs edit'>Edit </button> ;
      var deleteSpan = <button className='delete btn btn-danger btn-xs'>Delete </button>;
      if(campaign['interest'] !== undefined && campaign['interest']._id == data.interest){
        return(
          /*<li id={campaign._id}><div className='leftClass'><img src={campaign.image} width="50" height="50" />{campaign.name}</div><div className='rightClass'>{editSpan}{deleteSpan}</div></li>*/
          <li id={campaign._id}>
            <a href="#">
            <img className="dashboard-avatar" alt={campaign.name}
                src={campaign.image} /></a>
            <strong>Name:</strong> <a href="#">{campaign.name}
            </a><br />
            {editSpan} &nbsp; &nbsp;
            {deleteSpan}
            <br />
          </li>
        );
      }
    });
    return (
      <div className="box-content">
        <ul onClick={this.handleClick.bind(null)} className="dashboard-list">
          <li className='add' onClick={this.handleClick.bind(null)}> <label> Add new Campaign </label> </li>
          {campaignList}
        </ul>
      </div>

    );
  }
});

var CampaignModule = React.createClass({
  getInitialState: function() {
    return {
      name: '',
      image: '',
      yinkers: '',
      type: '',
      description: '',
      active: 'false',
      creator: 'admin',
      uploadFor: 'interest',
      tasks: '',
      interest: "",
      userData: this.props.userData,
      interestData: [], 
    };
  },
  handleSuccess: function(details){
    console.log(details);
  },

  handleError: function(xhr, status, err){
    console.error(this.props.url, status, err.toString());
    var partialState = {};
    partialState['message'] = "No";
    this.setState(partialState);
  },

  handleSubmit: function(e) {
    e.preventDefault();
    var name = this.state.name;
    var image = this.state.image;
    var yinkers = this.state.yinkers;
    var type = this.state.type;
    var description = this.state.description;
    var active = this.state.active;
    var creator = this.state.creator;
    var tasks = this.state.tasks;
    var interest_id = this.state.userData.interest;
    var campaignId = this.state.userData.campaign;
    var count = 0;
    var my_interest = '';
    var interest = this.state.userData.interestData.map(function(temp_interest){if(temp_interest._id == interest_id) my_interest = temp_interest; count++ });
    console.log(my_interest);
    //console.log(this.state.userData.interestData);
    interest = {name: my_interest.name, image: my_interest.image, _id: my_interest._id};
    var formData = {name:name, image: image, interest: interest, type: type, description: description, active: active, created_by: creator, access_token: this.props.userData.token, tasks: tasks};
    (campaignId != '') ? $.extend(formData,{_id: campaignId}) : "";
    this.handleCampaignSubmit(formData);
    return;
  },

  handleCampaignSubmit: function(formData) {
    this.handleSuccess(formData);
    $.ajax({
      type: 'POST',
      url: this.state.userData.serverURL + this.state.userData.campaignApi,
      dataType: 'application/json',
      data: formData,
      success: function(data) {
      this.handleSuccess(data);
      }.bind(this),
      error: function(xhr, status, err) {
        this.handleError(xhr, status, err);
      }.bind(this)
    });
  },

  handleInputChange: function(key, event) {
    var partialState = {};
    partialState[key] = (event.target.value);
    this.setState(partialState);
  },

  getInterestFromServer: function(){
    $.ajax({
      url: this.state.userData.serverURL+ this.state.userData.interestApi,
      type: 'GET',
      dataType: 'json',
      success: function(data) {
        console.log(data);
        this.setState({interestData: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  componentDidMount: function() {
    this.getInterestFromServer();
  },

  handleChange:function(e){
    console.log(e.target.value);
    this.setState({'interest':e.target.value});
  },

  componentWillMount: function() {
    var partialState = {};
    var campaignId = this.state.userData.campaign;
    var interest = this.state.interest;
    //var userData = this.state.userData;
    var InterestNodes = this.state.userData.interestData === undefined ? '' : this.state.userData.interestData.map(function (yinterest) {
      interest = interest == "" ? yinterest._id : interest;
    });
    var campaign = (campaignId != undefined && campaignId != "") ? this.getCampaignData() : '';
    partialState['name'] = (campaignId != undefined && campaignId != "") ? campaign.name : this.state.name;
    partialState['image'] = (campaignId != undefined && campaignId != "") ? campaign.image : this.state.image;
    partialState['type'] = (campaignId != undefined && campaignId != "") ? campaign.type : this.state.type;
    partialState['description'] = (campaignId != undefined && campaignId != "") ? campaign.description : this.state.description;
    partialState['active'] = (campaignId != undefined && campaignId != "") ? campaign.active : this.state.active;
    partialState['creator'] = (campaignId != undefined && campaignId != "") ? campaign.created_by : this.state.creator;
    partialState['tasks'] = (campaignId != undefined && campaignId != "") ? campaign.tasks : this.state.tasks;
    partialState['interest'] = (campaignId != undefined && campaignId != "") ? campaign.interest._id : interest;

    this.setState(partialState);
    console.log(campaign);
  },

  getCampaignData: function(){
    var taskId = this.state.userData.campaign;
    var taskData = this.state.userData.campaignData;
    var task = [];
    taskData.map(function(tempTask){
      if(tempTask._id == taskId){
        task = tempTask;
      }
    });
    return task;
  },

  submitForm: function(event){
    event.preventDefault();
    $.ajax({
      url: this.state.userData.S3UploadURL,
      type: 'POST',
      data: new FormData($('#uploadForm')[0]),
      async: false,
      cache: false,
      contentType: false,
      processData: false,
      success: function () {
          //alert('Form Submitted!');
      },
      error: function(err){
        //console.log(err);
          //alert("error in ajax form submission");
      }
    });
  },

  updateName: function(event)
  {
    var value = event.target.value.split(/[\/\\]/).pop();
    //$("#fileName").val(value);
    var suffix = '';
    var imagePath = this.state.userData.imageLocation + 'interests/';
    if(this.state.uploadFor == 'bounty'){
      suffix = 'bounty/bounty_';
      imagePath += suffix;
      this.state.bimage = imagePath+value;
    }
    else{
      this.state.image=imagePath+value;
    }
    var FILE = "interests/" + suffix + value;
    var POLICY = '{   "expiration": "'+new Date(Date.now()+31536000000).toISOString()+'",  "conditions": [     {"bucket": "' + this.state.userData.BUCKET + '" },     {"acl": "public-read" },     ["eq", "$key", "' + FILE + '"],     ["starts-with", "$Content-Type", "image"],   ] } ';

    var awsid = this.state.userData.AWS_KEY;
    var awskey = this.state.userData.AWS_SECRET;

    var policyText = POLICY.toString();
    var policyBase64 = Base64.encode(policyText);

    var signature = b64_hmac_sha1(awskey, policyBase64);

    this.state.userData['FILE'] = FILE;

    this.state.userData['policy'] = policyBase64;
    this.state.userData['signature'] = signature;

    this.setState(this.state);
  },

  render: function() {
    var name = this.state.name;
    var image = this.state.image;
    var type = this.state.type;
    var description = this.state.description;
    var active = this.state.active;
    var creator = this.state.creator;
    var tasks = this.state.tasks;
    var interest = this.state.interest;
    var fileName = this.state.userData.FILE;
    var AWSAccessKeyId = this.state.userData.AWS_KEY;
    var policy = this.state.userData.policy;
    var uploadFor = this.state.uploadFor;
    var signature = this.state.userData.signature;
    
    var InterestNodes = this.state.userData.interestData === undefined ? '' : this.state.userData.interestData.map(function (yinterest) {
      //interest = interest == "" ? yinterest._id : interest;
      return (
        <option value={yinterest._id}>
          {yinterest.name}
        </option>
      );
    });

    var showImage = "";
    if(image != ''){
      showImage = <div> <img src={image} width="50" height="50"/></div>;
    }
    
    return (
      <div>
    	  <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label for="name">Name: </label>
            <input type="text" className="form-control" id="name" placeholder="#000000" value={name} onChange={this.handleInputChange.bind(null, 'name')} />
          </div>
          <div className="form-group">
            <label for="image">Image: </label>
            <input type="text" className="form-control" id="image" placeholder="#000000" value={image} onChange={this.handleInputChange.bind(null, 'image')} />
            {showImage}
          </div>
          <div className="form-group">
            <label for="type">Type </label>
            <input type="text" className="form-control" id="type" placeholder="#000000" value={type} onChange={this.handleInputChange.bind(null, 'type')} />
          </div>
          <div className="form-group">
            <label for="description">Description </label>
            <input type="text" className="form-control" id="description" placeholder="#000000" value={description} onChange={this.handleInputChange.bind(null, 'description')} />
          </div>
          <div className="form-group">
            <label for="tasks">Tasks </label>
            <input type="text" className="form-control" id="tasks" placeholder="#000000" value={tasks} onChange={this.handleInputChange.bind(null, 'tasks')} />
          </div>
          <div className="form-group">
            <input type="hidden" value={active} onChange={this.handleInputChange.bind(null, 'active')} />
            <input type="hidden" value={creator} onChange={this.handleInputChange.bind(null, 'creator')} />
            <input type="submit" value='Add' className="btn btn-default"/>
          </div>
        </form>
        <div id='uploadImageDiv'>
          <label>Upload Image:</label>
          <form id="uploadForm" action="javascript:;" method="post" encType="multipart/form-data" onSubmit={this.submitForm}>
            <input type="hidden" id="fileName" name="key" value={fileName} />
            <input type="hidden" name="acl" value="public-read" />
            <input type="hidden" name="content-type" value="image/png" />
            <input type="hidden" name="AWSAccessKeyId" value={AWSAccessKeyId} />
            <input type="hidden" id="policy" name="policy" value={policy} />
            <input type="hidden" id="signature" name="signature" value={signature} />
            <input id="file" name="file" type="file" onChange={this.updateName.bind(null)} accept="image/*"/>
            <input name="submit" className='btn btn-success btn-sm' value="Upload" type="submit" onSubmit={this.submitForm}/>
          </form>
          </div>
      </div>

    );
  }
});


